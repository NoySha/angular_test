import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { ModelSailorComponent } from './model-sailor/model-sailor.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { StudentComponent } from './student/student.component';
import { TableCustomerComponent } from './table-customer/table-customer.component';
import { TableStudentComponent } from './table-student/table-student.component';




const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignUpComponent },
  { path: 'student', component: StudentComponent },
  { path: 'studentTable', component: TableStudentComponent },
  { path: 'sailor', component: ModelSailorComponent },
  { path: 'customerTable', component: TableCustomerComponent }




];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
