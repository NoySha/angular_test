import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class TableCustomerService {
  customersCollection:AngularFirestoreCollection;


  getCustomers(){
    this.customersCollection = this.db.collection(`customers`, 
    ref => ref);
    return this.customersCollection.snapshotChanges()
  }

  
  updateCustomer(CustomerId:string, predict:string){
    this.db.doc(`customers/${CustomerId}`).update(
      {
        predict:predict
      }
    )
  }

  constructor(private db:AngularFirestore) { }


}
