import { Component, OnInit } from '@angular/core';
import { AuthService } from './../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email;
  password;
  isError:boolean = false;
  errorMessage:string;


  onSubmit(){
    this.AuthService.login(this.email, this.password).then(
      res => { 
      console.log(res);
      this.router.navigate(['/home']); 
  }
  ).catch(
    err => { // err הוא אובייקט שמכיל את הודעת השגיאה
      console.log(err);
      this.isError = true;
      this.errorMessage = err.message; // רואים בקונסול שלפונקציה ארר יש שדה שנקרא message
    }
  )
}

  constructor(private AuthService:AuthService, private router:Router) { }

  ngOnInit(): void {
  }

}
