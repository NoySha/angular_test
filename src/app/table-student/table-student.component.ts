import { TableStudentService } from './../table-student.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from './../auth.service';


@Component({
  selector: 'app-table-student',
  templateUrl: './table-student.component.html',
  styleUrls: ['./table-student.component.css']
})
export class TableStudentComponent implements OnInit {

  students=[];
  displayedColumns: string[] = ['mathAverage', 'psychometric','paid', 'result', 'userEmail', 'delete'];
  userId;

  delete(index){
    let id = this.students[index].id;
    this.TableStudentService.deleteStudent(this.userId, id)
  }

  constructor(public AuthService:AuthService, private TableStudentService:TableStudentService) { }

  ngOnInit(): void {
    this.AuthService.getUser().subscribe(
      user => {
      this.userId = user.uid;
      this.TableStudentService.getStudents(this.userId).subscribe(
        students =>{
        this.students = students
        console.log(this.userId)
        console.log(this.students)

      })
      })
  }


}
