export interface Customer {
    id:string,
    age:number,
    more_than_1_card:number,
    student:number,
    main_city_sail:number,
    Utilizing_card_1:number,
    Utilizing_card_2:number,
    Utilizing_card_3:number,
    Utilizing_card_4:number,
    perc_private:number,
    perc_proff:number,
    predict?:string
}
