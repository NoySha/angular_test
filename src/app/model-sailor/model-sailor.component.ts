import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import { VERSION ,ViewChild } from '@angular/core';


@Component({
  selector: 'app-model-sailor',
  templateUrl: './model-sailor.component.html',
  styleUrls: ['./model-sailor.component.css']
})


export class ModelSailorComponent implements OnInit {
  resultPredict;
  age:number;
  more_than_1_card:number;
  main_city_sail:number;
  student:number;
  Utilizing_card_1:number;
  Utilizing_card_2:number;
  Utilizing_card_3:number;
  Utilizing_card_4:number;
  perc_private:number;
  perc_proff:number;



  predict(){
    this.ClassifyService.classify(this.age,this.more_than_1_card, this.main_city_sail, this.student, this.Utilizing_card_1, this.Utilizing_card_2, this.Utilizing_card_3, this.Utilizing_card_4, this.perc_private, this.perc_proff).subscribe(
      res=> {
        console.log(res);
        if (res > 0.5){
          this.resultPredict = 'will not leave';
        }
        if (res < 0.5) {
          this.resultPredict = 'will leave';
        }
        else{
          console.log('else');

        }
      }
    )
  }

  




  constructor(private ClassifyService:ClassifyService) { }

  ngOnInit(): void {
  }

}
