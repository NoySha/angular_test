import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';



@Injectable({
  providedIn: 'root'
})
export class StudentService {
  private url = "https://cuvzm3lyi6.execute-api.us-east-1.amazonaws.com/test"

  classify(mathAverage:number, psychometric:number, paid:boolean):Observable<any>{
    let json = {
      'data': {
        'mathAverage': mathAverage,
        'psychometric': psychometric,
        'paid' : paid
      }
    }
    let body = JSON.stringify(json);
    return this.HttpClient.post<any>(this.url, body).pipe(
      map(res => {
        console.log(res); 
        let final:string = res.body;
        return final; 
      })
      )
  
    }

    addStudent(userId, mathAverage, psychometric, paid, result, userEmail, index){
      this.db.collection(`users/${userId}/students`).add(
        {mathAverage:mathAverage, psychometric:psychometric, paid:paid, result:result, userEmail:userEmail, index:index}
      )
    }

  constructor(private HttpClient:HttpClient, private db:AngularFirestore) { }
}
