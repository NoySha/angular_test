import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ClassifyService {

  //Api get way URL (connect to lamda function call "test")
  private url = "https://2n661lzq2m.execute-api.us-east-1.amazonaws.com/model"

  
  classify(age:number, more_than_1_card:number,main_city_sail:number,student:number,Utilizing_card_1:number,Utilizing_card_2:number,Utilizing_card_3:number,Utilizing_card_4:number,perc_private:number,perc_proff:number):Observable<any>{
      const t = 0;
      const v = 0;
      const b = 0;
      const c = 0;

      let json = {
        'data': [ { 
          'age': t,
          'more_than_1_card': v,
          'main_city_sail': b,
          'student': c,
          'Utilizing_card_1': Utilizing_card_1,
          'Utilizing_card_2': Utilizing_card_2,
          'Utilizing_card_3': Utilizing_card_3,
          'Utilizing_card_4': Utilizing_card_4,
          'perc_private': perc_private,
          'perc_proff': perc_proff
        }
      ]
      }

      let body = JSON.stringify(json);
      console.log(body)
   return this.HttpClient.post<any>(this.url, body).pipe(
        map(res => {
          console.log(res); 
          let final:string = res.body;
          //final = final.replace('[',''); // אנחנו רוצים להוריד את הסוגריים מהמספר שמוחזר מהבודי
          //final = final.replace(']',''); // אנחנו רוצים להוריד את הסוגריים מהמספר שמוחזר מהבודי
          return final; 
        })
      )
  
    }


  constructor(private HttpClient:HttpClient) { }
}
