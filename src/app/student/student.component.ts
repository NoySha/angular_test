import { StudentService } from './../student.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from './../auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {
  mathAverage:number;
  psychometric:number;
  paid:boolean;
  paids = [true,false];
  resultPredict;
  prdictClick = false;
  cancelClick;
  resultes = ['will not leave','will leave'];
  userId;
  userEmail;
  errorOrSaved;
  index=30;


  Add(){

  }

  predict(){
    this.StudentService.classify(this.mathAverage,this.psychometric, this.paid).subscribe(
      res=> {
        if (res > 0.5){
          this.resultPredict = 'will not leave';
        }
        else {
          this.resultPredict = 'will leave';
        }
      }
    )
  }

  checkValid(){
    if (this.mathAverage>=0 && this.mathAverage<=100 && this.psychometric>=0 && this.psychometric<=800){
      this.save()
      this.errorOrSaved = 'saved successfully'
    }
    else{
      this.errorOrSaved = 'mathAverage must be between 0-100 and psychometric must be between 0-800';
    }

  } 

  save(){
    this.StudentService.addStudent(this.userId,this.mathAverage, this.psychometric, this.paid, this.resultPredict, this.userEmail, this.index)
    this.router.navigate(['/studentTable']);

  }




  constructor(private StudentService:StudentService, public AuthService:AuthService, private router:Router) { }

  ngOnInit(): void {
    this.AuthService.getUser().subscribe(
      user => {
      this.userId = user.uid;
      this.userEmail = user.email;
      console.log(user.email)
      })
  }

}
