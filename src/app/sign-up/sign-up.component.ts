import { Router } from '@angular/router';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  email:string;
  password: string;
  errorMessage:string;
  isError:boolean = false;
 
  onSubmit(){
    this.AuthService.signUp(this.email, this.password).then(
      res => { 
      console.log('successful login;');
      this.Router.navigate(['/home']); 
  }
  ).catch(
    err => { // err הוא אובייקט שמכיל את הודעת השגיאה
      console.log(err);
      this.isError = true;
      this.errorMessage = err.message; // רואים בקונסול שלפונקציה ארר יש שדה שנקרא message
    }
  )
}

  constructor(private AuthService:AuthService, private Router:Router) { }

  ngOnInit(): void {
  }

}
