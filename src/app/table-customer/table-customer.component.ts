import { ClassifyService } from './../classify.service';
import { TableCustomerService } from './../table-customer.service';
import { Component, OnInit } from '@angular/core';
import { Customer } from '../customer';

@Component({
  selector: 'app-table-customer',
  templateUrl: './table-customer.component.html',
  styleUrls: ['./table-customer.component.css']
})
export class TableCustomerComponent implements OnInit {

  customers=[];
  displayedColumns: string[] = ['age', 'more_than_1_card','main_city_sail', 'student', 'Utilizing_card_1', 'Utilizing_card_2','Utilizing_card_3','Utilizing_card_4','perc_private','perc_proff'];
  customers$;
  new;
  resultPredict;

  try(){
      this.TableCustomerService.updateCustomer('NCwnAqyB7q2V4CdjQcG9', 'jg');            
  }
  

  constructor(private TableCustomerService:TableCustomerService, private ClassifyService:ClassifyService) { }

  ngOnInit() {
    this.customers$ = this.TableCustomerService.getCustomers(); 
    this.customers$.subscribe(
      docs =>{
        console.log(docs);
        this.customers = [];
        for(let document of docs){
          console.log(document);
          const customer:Customer= document.payload.doc.data();
          customer.id = document.payload.doc.id; 
          this.ClassifyService.classify(customer.age,customer.more_than_1_card, customer.main_city_sail, customer.student, customer.Utilizing_card_1, customer.Utilizing_card_2, customer.Utilizing_card_3, customer.Utilizing_card_4, customer.perc_private, customer.perc_proff).subscribe(
            res=> {
              console.log(res);
              if (res > 0.5){
                customer.predict = 'will not leave';
              }
              if (res < 0.5) {
                customer.predict = 'will leave';
              }
            }
          )
        this.customers.push(customer);
        this.TableCustomerService.updateCustomer('NCwnAqyB7q2V4CdjQcG9', 'jg');            
  
        }
        

      }
    )
      
  }
}




