import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { User } from './interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user:Observable<User | null>; //יצרנו אינטרפייס של יוזר. האובסרבל יהיה מסוג יוזר או מסוג נאל


  logout(){
    this.afAuth.signOut();
  }

  login(email:string, password:string){
    return this.afAuth.signInWithEmailAndPassword(email,password)
  }

  getUser():Observable<User | null>{
    return this.user;
    // הפונקציה מחזירה אובסרבל של יוזר שמחובר כעת, ואם אין היא מחזירה נאל
  }

  signUp(email:string, password:string){
    return this.afAuth.createUserWithEmailAndPassword(email,password)
  }


  constructor(private afAuth:AngularFireAuth) { 
    this.user = this.afAuth.authState; // יצרנו חיבור בין היוזר לבין האובסרבל 

  }
}
