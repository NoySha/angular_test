// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBR32YCUsE2_ivTJEy4e2dOJS4Sh69AaAs",
    authDomain: "test-noy.firebaseapp.com",
    projectId: "test-noy",
    storageBucket: "test-noy.appspot.com",
    messagingSenderId: "803734118651",
    appId: "1:803734118651:web:8f6b5af6fd08f1c8d8505e"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
